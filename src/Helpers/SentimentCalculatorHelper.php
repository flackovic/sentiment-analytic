<?php
/**
 * Created by PhpStorm.
 * User: flackovic
 * Date: 24/10/2018
 * Time: 00:14.
 */

namespace App\Helpers;

use App\DTO\SentimentDataDto;

class SentimentCalculatorHelper
{
    public function calculateSentiment(array $sentimentData): int
    {
        $positive = 0;
        $total = 0;

        /** @var SentimentDataDto $sentimentDataDto */
        foreach ($sentimentData as $sentimentDataDto) {
            $positive += $sentimentDataDto->getPositiveCount();
            $total += $sentimentDataDto->getTotalCount();
        }

        return $this->getScore($positive, $total);
    }

    public function getScore($positive, $total)
    {
        if (0 === $total) {
            return 0;
        }

        return round(($positive / $total) * 100, 2) / 10;
    }
}
