<?php

namespace App\Controller;

use App\Repository\TermRepository;
use App\Service\SentimentAnalyzerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    /**
     * @Route("/score", name="score", methods={"GET", "OPTIONS"})
     */
    public function getScore(Request $request, TermRepository $termRepository, SentimentAnalyzerService $analyzerService)
    {
        if (!$searchTerm = $request->query->get('term')) {
            return new JsonResponse(['error' => 'Missing parameter term'], 400);
        }

        if (!$term = $termRepository->findOneOrNullByTerm($searchTerm)) {
            $term = $analyzerService->analyzeTerm($searchTerm);
        }

        return new JsonResponse([
            'term' => $term->getTerm(),
            'score' => $term->getSentiment(),
        ]);
    }
}
