<?php

namespace App\Repository;

use App\Entity\Term;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Term|null find($id, $lockMode = null, $lockVersion = null)
 * @method Term|null findOneBy(array $criteria, array $orderBy = null)
 * @method Term[]    findAll()
 * @method Term[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TermRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Term::class);
    }

    public function save(Term $term): ?Term
    {
        $this->_em->persist($term);
        $this->_em->flush();

        return $term;
    }

    /**
     * @param string $term
     *
     * @return Term|null
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneOrNullByTerm(string $term): ?Term
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.term = :term')
            ->setParameter('term', $term)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }
}
