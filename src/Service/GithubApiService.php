<?php

namespace App\Service;

use App\Interfaces\SearchableApiInterface;

class GithubApiService extends BaseApiService implements SearchableApiInterface
{
    const BASE_URL = 'https://api.github.com';
    const SEARCH_ISSUES_URL = '/search/issues';

    /** @var string */
    private $apiToken;

    public function __construct()
    {
        parent::__construct();

        $this->apiToken = getenv('GITHUB_API_TOKEN');
    }

    public function search(string $keyword): \stdClass
    {
        $params = [
            'access_token' => $this->apiToken,
            'q' => $keyword,
            'in:title,body',
        ];

        $response = $this->get(self::BASE_URL.self::SEARCH_ISSUES_URL, $params);

        return json_decode($response->getBody()->getContents());
    }

    public function resultsCount(string $keyword): int
    {
        return $this->search($keyword)->total_count;
    }
}
