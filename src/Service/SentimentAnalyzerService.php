<?php
/**
 * Created by PhpStorm.
 * User: flackovic
 * Date: 23/10/2018
 * Time: 22:03.
 */

namespace App\Service;

use App\DTO\SentimentDataDto;
use App\Entity\Term;
use App\Helpers\SentimentCalculatorHelper;
use App\Interfaces\SearchableApiInterface;
use App\Repository\TermRepository;

/**
 * Sentiment Analyzer will iterate through
 * all registered services and calculate sentiment data
 * for given search term.
 *
 * Register new api service by adding it to constructor.
 * Note: api service must implement SearchableApiInterface
 *
 * Class SentimentAnalyzerService
 */
class SentimentAnalyzerService
{
    const SENTIMENT_TYPE_POSITIVE = 'rocks';
    const SENTIMENT_TYPE_NEGATIVE = 'sucks';

    /** @var TermRepository */
    private $repository;
    /** @var array */
    private $registeredApiServices;
    /** @var SentimentCalculatorHelper */
    private $sentimentCalculatorHelper;

    /**
     * @var SentimentDataDto[]
     */
    private $resultsData;

    public function __construct(TermRepository $repository)
    {
        $this->repository = $repository;
        $this->sentimentCalculatorHelper = new SentimentCalculatorHelper();

        $this->registeredApiServices = [
            GithubApiService::class,
        ];
    }

    /**
     * @param string $term
     *
     * @throws \Exception
     */
    public function analyzeTerm(string $term): Term
    {
        $this->getSentimentDataFromServices($term);

        $sentiment = $this->sentimentCalculatorHelper->calculateSentiment($this->resultsData);

        $term = $this->saveResults($term, $sentiment);

        return $term;
    }

    /**
     * @param string $term
     *
     * @throws \Exception
     */
    private function getSentimentDataFromServices(string $term): array
    {
        foreach ($this->registeredApiServices as $registeredApiService) {
            $sentimentDataDto = new SentimentDataDto();

            /** @var SearchableApiInterface $apiService */
            $apiService = new $registeredApiService();

            $positiveResultsCount = $apiService->resultsCount($this->prepareSearchQuery($term, self::SENTIMENT_TYPE_POSITIVE));
            $negativeResultsCount = $apiService->resultsCount($this->prepareSearchQuery($term, self::SENTIMENT_TYPE_NEGATIVE));

            $sentimentDataDto->addPositive($positiveResultsCount);
            $sentimentDataDto->addNegative($negativeResultsCount);

            $this->resultsData[] = $sentimentDataDto;
        }

        return $this->resultsData;
    }

    /**
     * @param string $term
     * @param $sentiment
     *
     * @return Term|null
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function saveResults(string $term, $sentiment)
    {
        /*
         * This should not happen, but lets be absolutely sure we do not have
         * that term already in database.
         */
        if (!$termObject = $this->repository->findOneOrNullByTerm($term)) {
            $termObject = new Term();
            $termObject->setTerm($term);
        }

        $termObject->setSentiment($sentiment);

        return $this->repository->save($termObject);
    }

    /**
     * @param string $keyword
     * @param string $sentimentType
     *
     * @return string
     */
    private function prepareSearchQuery(string $keyword, string $sentimentType): string
    {
        return sprintf('%s %s', $keyword, $sentimentType);
    }
}
