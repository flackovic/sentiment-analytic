<?php

namespace App\Service;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

abstract class BaseApiService
{
    /** @var Client */
    protected $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    protected function get(string $url, $params = []): ResponseInterface
    {
        return $this->client->get($url, ['query' => $params]);
    }
}
