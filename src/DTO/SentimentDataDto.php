<?php
/**
 * Created by PhpStorm.
 * User: flackovic
 * Date: 23/10/2018
 * Time: 22:18.
 */

namespace App\DTO;

use App\Exceptions\InvalidValueException;

/**
 * DTO for transfering/holding sentiment data.
 *
 * Class SentimentDataDTO
 */
class SentimentDataDto
{
    private $positiveCount = 0;
    private $negativeCount = 0;

    /**
     * @param int $count
     *
     * @return SentimentDataDto
     *
     * @throws \Exception
     */
    public function addPositive(int $count): self
    {
        $this->guardAgainstInvalidValue($count);

        $this->positiveCount += $count;

        return $this;
    }

    /**
     * @param int $count
     *
     * @return SentimentDataDto
     *
     * @throws \Exception
     */
    public function addNegative(int $count): self
    {
        $this->guardAgainstInvalidValue($count);

        $this->negativeCount += $count;

        return $this;
    }

    /**
     * @return int
     */
    public function getPositiveCount(): int
    {
        return $this->positiveCount;
    }

    /**
     * @return int
     */
    public function getNegativeCount(): int
    {
        return $this->negativeCount;
    }

    /**
     * @return int
     */
    public function getTotalCount(): int
    {
        return $this->getPositiveCount() + $this->getNegativeCount();
    }

    /**
     * @param int $count
     *
     * @throws \Exception
     */
    private function guardAgainstInvalidValue(int $count)
    {
        if ($count < 0) {
            throw new InvalidValueException('Invalid value provided!');
        }
    }
}
