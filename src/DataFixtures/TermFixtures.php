<?php

namespace App\DataFixtures;

use App\Entity\Term;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class TermFixtures extends Fixture
{
    private $termData;

    public function __construct()
    {
        $this->termData = [
            [
                'term' => 'php',
                'sentiment' => 3.33,
            ],
            [
                'term' => 'js',
                'sentiment' => 4.13,
            ],
            [
                'term' => 'doctrine',
                'sentiment' => 5.35,
            ],
            [
                'term' => 'linus',
                'sentiment' => 1.23,
            ],
        ];
    }

    public function load(ObjectManager $manager)
    {
        foreach ($this->termData as $termDatum) {
            $term = new Term();
            $term->setTerm($termDatum['term']);
            $term->setSentiment($termDatum['sentiment']);

            $manager->persist($term);
        }

        $manager->flush();
    }
}
