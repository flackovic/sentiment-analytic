<?php
/**
 * Created by PhpStorm.
 * User: flackovic
 * Date: 23/10/2018
 * Time: 22:11.
 */

namespace App\Interfaces;

/**
 * Sets methods that api service should
 * implement if it is planned to
 * use it in SentimentAnalyzer.
 *
 * Class SearchableApiInterface
 */
interface SearchableApiInterface
{
    public function search(string $keyword);

    public function resultsCount(string $keyword): int;
}
