<?php

namespace App\Tests\Unit;

use App\Helpers\SentimentCalculatorHelper;
use PHPUnit\Framework\TestCase;

class SentimentCalculatorHelperTest extends TestCase
{
    /** @var SentimentCalculatorHelper */
    private $sentimentCalculator;

    public function setUp()
    {
        $this->sentimentCalculator = new SentimentCalculatorHelper();
    }

    public function testItWillReturnZeroWhenTotalIsZero()
    {
        $score = $this->sentimentCalculator->getScore(0, 0);

        $this->assertSame(0, $score);
    }

    /**
     * @param $positive
     * @param $total
     * @param $expected
     *
     * @dataProvider provideSentimentData
     */
    public function testItWillReturnCorrectScore($positive, $total, $expected)
    {
        $score = $this->sentimentCalculator->getScore($positive, $total);

        $this->assertSame($expected, $score);
    }

    public function provideSentimentData()
    {
        return [
            [5, 10, 5.00],
            [10, 10, 10.00],
            [1, 10, 1.00],
            [0, 100, 0.00],
            [1, 100, 0.10],
            [10, 10, 10.00],
            [3, 10, 3.00],
        ];
    }
}
