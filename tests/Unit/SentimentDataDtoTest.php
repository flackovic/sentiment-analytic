<?php

namespace App\Tests\Unit;

use App\DTO\SentimentDataDto;
use App\Exceptions\InvalidValueException;
use PHPUnit\Framework\TestCase;

class SentimentDataDtoTest extends TestCase
{
    /** @var SentimentDataDto */
    private $sentimentDataDto;

    public function setUp()
    {
        $this->sentimentDataDto = new SentimentDataDto();
    }

    public function testPositiveCountWillBeZeroByDefault()
    {
        $this->assertSame(0, $this->sentimentDataDto->getPositiveCount());
    }

    public function testNegativeCountWillBeZeroByDefault()
    {
        $this->assertSame(0, $this->sentimentDataDto->getNegativeCount());
    }

    /**
     * @dataProvider provideValidResultsCountData
     */
    public function testAddPositiveWillCorrectlyIncrementPositiveCount($count, $expectedPositiveCount)
    {
        $this->sentimentDataDto->addPositive($count);

        $this->assertSame($expectedPositiveCount, $this->sentimentDataDto->getPositiveCount());
        $this->assertSame(0, $this->sentimentDataDto->getNegativeCount());
    }

    /**
     * @dataProvider provideValidResultsCountData
     */
    public function testAddNegativeWillCorrectlyIncrementNegativeCount($count, $expectedNegativeCount)
    {
        $this->sentimentDataDto->addNegative($count);

        $this->assertSame($expectedNegativeCount, $this->sentimentDataDto->getNegativeCount());
        $this->assertSame(0, $this->sentimentDataDto->getPositiveCount());
    }

    /**
     * @dataProvider provideInvalidResultsCount
     */
    public function testAddPositiveWillThrowExceptionWhenInvalidValueProvided($count)
    {
        $this->expectException(InvalidValueException::class);
        $this->sentimentDataDto->addPositive($count);
    }

    /**
     * @dataProvider provideInvalidResultsCount
     */
    public function testAddNegativeWillThrowExceptionWhenInvalidValueProvided($count)
    {
        $this->expectException(InvalidValueException::class);
        $this->sentimentDataDto->addNegative($count);
    }


    public function provideValidResultsCountData()
    {
        return [
            [0, 0],
            [1, 1],
            [100, 100],
            [100000, 100000],
        ];
    }

    public function provideInvalidResultsCount()
    {
        return [
            [-1],
            [-100],
            [-10000000],
        ];
    }
}
