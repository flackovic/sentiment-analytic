# Sentiment Analytic

## Project setup

- Clone repo

- Install dependencies (composer install)

- Create .env file from dist (cp .env.dist .env)

- Populate .env file with DB credentials and your Github API token

- Create database (php bin/console doctrine:database:create)

- Migrate database (php bin/console doctrine:migrations:migrate)

- Optional: seed dummy data (php bin/console doctrine:fixtures:load)

- Serve project (php bin/console server:run)

## API
We have simple ApiController that serves as entry point for external requests.
It accepts GET and OPTIONS requests.
Currently it has no validations, nor authentications.

[Apiary docs](https://sentimentanalysis7.docs.apiary.io)


## Dev

####Services

###### BaseApi
Used as a holder for basic http communication. It is platform independent.

###### GithubApi
Layer around BaseApi that holds some platform specific logic such as base url, auth and response handling

###### AnalyzerService
Main service that loads each of registered api services and performs search.
After term is analyzed, results are saved to database so we do not need to perform api calls every time since whole process could be costly.


####DTOs

######SentimentDataDto
Simple DTO used for data transfer since we will have multiple APIs returning different responses, and we need some sort of data normalization.


####Entities

######Term
Main, and only, entity that holds actual search term and its sentiment.
Term is not platform specific, for that we should create OneToMany relation that will hold platform specific data.


####Future development

###### How to add new api service / platform?

Every api service planned to be used as a search provider should first implement SearchableApiInterface.
Reason behind that is so we can assure methods used in our SentimentAnalyzer (eg. searchCount) is implemented.

Api service should be "registered" inside SentimentAnalyzer, after that it will be active.

Repro steps:

- Create api service (it should, but it does not need to, extend BaseApiService)
- Implement SearchableApiInterface
- Register your new service under the registeredApiServices inside SentimentAnalyzer

Api authentication and response handling is up to you, since it can heavily differ from platform to platform
